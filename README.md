# README #

## Description 

This repo contains a single python3 file rmdup. It lists and deletes files that are duplicates in the directory it is executed or in the directory or directories given to it as arguments.

The version that is kept after deletion is the first path found when recursing the directory structure in a depth first search. To give an alternative to that there is the sort option that keeps the first of the sorted full paths of the duplicates. That option supports the use case where the directories are named with iso dates like `2017-07-11`. The duplicate with the lowest date will then be kept. Useful when cleaning photo albums.

Download it, make it executable, put it on your path or point to it to run it.

## Usage

    rmdup [-h] [-n] [-v] [directory ...]

    -h This usage
    -n Dry run
    -v Verbose
    -s Sort paths alphabetically - keep first
	
## Bugs

There is only two ways to control which duplicate will be kept. Either the file found first or, when usign the sort option, the file which name sorted in dictionary order comes first among the duplicates.

## Tips

Use the dry-run option `-n` to check what will be deleted before actually doing it.
